# This program is used to test the integration between PULSE and JIRA

import pypyodbc
import requests
import xmltodict
import json
import sys
import os
import configparser
from myLog import MyLog

# Project initialization - global variables

Prj_mapping = ""                    # The folder where project mapping file locates
Jira_projectID = ""                 # Jira project ID where the issue/story is going to create
Jira_projectName = ""               # Jira project name used for component setting
Jira_issueID = "--"                 # Created JIRA issue/story ID from response
Jira_affected = ""                  # PULSE affected user --> JIRA User Deatils (custom)
Jira_business_contact = ""          # PULSE reporting user --> JIRA Business Contact (custom)
Jira_summary = ""                   # PULSE summary --> JIRA summary
Jira_description = ""               # PULSE description --> JIRA description
Jira_component = []                 # PULSE items  -->  JIRA component (accept only array)
Jira_datelogged = ""                # PULSE date.time logged  --> Jira Start Date
Jira_business_priority = 0          # PULSE business planning priority P1~P4, P0 = N/A
Jira_business_impact = ""           # PULSE business impact --> JIRA business rationale (custom)
Jira_implementation_grouping = ""   # PULSE implementation grouping --> release or version
Jira_user_details = ""              # PULSE affected user details --> Jira user_details
Jira_workaround_description = ""    # PULSE workaround description --> new field (custom)
Jira_workaround_owner = ""          # PULSE workaround owner --> new field (custom)
Jira_work_type = []                 # "Incicent", "Problem" or "Service Request" --> Jira Work Type field
Jira_url = ""                       # Jira rest api http URL server part
Jira_epic = ""                      # Jira epic from mapping file
Jira_basic_auth = ""                # 1043-SVC-PULSEAD-PRD or *albert.hsieh@innovapost.com API token
PULSE_basic_auth = ""               # zzeng(FDDWD756756snsns) or *Albert.Hsieh or zzeng(PULSE4LIFE2014??)
PULSE_SQL_server = ""               # PULSE SQL server
PULSE_SQL_database = ""             # PULSE SQL database
PULSE_SQL_uid = ""                  # PULSE SQL username
PULSE_SQL_pwd = ""                  # PULSE SQL password
PULSE_csg = ""                      # PULSE affected user CSG
PULSE_url = ""                      # PULSE rest api http url server part
PULSE_ldap_url = ""                 # PULSE URL to go through ldap authentication not localhost for attachment and event URLs
PULSE_ldap_cpc_url = ""             # PULSE CPC URL to go through ldap authentication not localhost for attachment URL
PULSE_ldap_inn_url = ""             # PULSE INN URL to go through ldap authentication not localhost for attachment URL
PULSE_ldap_pur_url = ""             # PULSE PUR URL to go through ldap authentication not localhost for attachment URL
PULSE_ldap_sci_url = ""             # PULSE SCI URL to go through ldap authentication not localhost for attachment URL
PULSE_items = []                    # PULSE item (itemA + linked items)
PULSE_eventProduct = ""             # PULSE Product used to get the corresponding JIRA project from mapping file
PULSE_eventID = ""                  # PULSE eventID ex: 1xxx->incident, 2xxx->problem, 4xxx->Change, 8xxx->task, 16xxx-> decision, 32-->auth task
PULSE_eventURL = ""                 # PULSE event URL to connect back to the incident
PULSE_issueID = "--"                # value of field Jira Issue ID in Incident form
PULSE_eventREF = ""                 # PULSE event reference ex: xxxxxx, Sxxxxxx, Rxxxxxx,...
PULSE_description = ""              # PULSE current description
PULSE_shortdesc = ""                # PULSE short description (summary)
PULSE_formID =""                    # PULSE incident/problem form ID - column ent_id of DB table inc_data
Error_Integration = ""              # Treat as an error flag and hold the error message (SQL query, JIRA Rest API exception information)
Close_Event_Flag = ""               # Flag to hold the third argument whether to close event Close/Export

# Initialize Python Logging Feature

mylog=MyLog()
# mylog.disable()


# Get PULSE SQL server, url information and JIRA url information from config file

def Get_Config_Info ():

    global PULSE_SQL_server
    global PULSE_SQL_database
    global PULSE_SQL_uid
    global PULSE_SQL_pwd
    global PULSE_url
    global PULSE_ldap_cpc_url
    global PULSE_ldap_inn_url
    global PULSE_ldap_pur_url
    global PULSE_ldap_sci_url
    global PULSE_basic_auth
    global PULSE_logfolder
    global Jira_url
    global Jira_basic_auth
    global Prj_mapping

    try:
        mylog.info("--> Get program configuration file")
        with open("../PULSE_JIRA_CLOUD_Integration/"+"PULSE_JIRA_Config.txt") as fp:
        #with open("PULSE_JIRA_Config.txt") as fp:
            config = configparser.ConfigParser()
            # config.readfp(fp)
            config.read_file(fp)
    except configparser.Error:
        mylog.error("Error: Failed to retrieve configuration file. Please check the path and name of the file")

    PULSE_SQL_server = config.get('Database', 'SQL_Server')
    PULSE_SQL_database = config.get('Database', 'SQL_Database')
    PULSE_SQL_uid = config.get('Database', 'SQL_UID')
    PULSE_SQL_pwd = config.get('Database', 'SQL_PWD')
    PULSE_url = config.get('PULSE', 'URL')
    PULSE_ldap_cpc_url = config.get('PULSE', 'LDAP_CPC_URL')
    PULSE_ldap_inn_url = config.get('PULSE', 'LDAP_INN_URL')
    PULSE_ldap_pur_url = config.get('PULSE', 'LDAP_PUR_URL')
    PULSE_ldap_sci_url = config.get('PULSE', 'LDAP_SCI_URL')
    PULSE_basic_auth = "Basic " + config.get('PULSE', 'Basic_Auth')
    Jira_basic_auth = "Basic " + config.get('JIRA', 'Basic_Auth')
    Jira_url = config.get('JIRA', 'URL')

# Locate the JIRA project ID and name from data file (format: PULSE Product(no space)+ space + Jira project ID#Jira project Name)

def GetJiraProjects ():
    filename = "../PULSE_JIRA_CLOUD_Integration/" + 'JiraProjects.txt'
    #filename = 'JiraProjects.txt'
    with open(filename) as prj:
        return dict(line.rsplit(None, 1) for line in prj)


# Locate PULSE Incident ID from action_registry_ID using SQL command

def Get_PULSE_Event_Data ():

    global Jira_eventID
    global Jira_issueID
    global Jira_projectID
    global Jira_projectName
    global Jira_business_contact
    global Jira_summary
    global Jira_component
    global Jira_description
    global Jira_datelogged
    global Jira_implementation_grouping
    global Jira_business_priority
    global Jira_business_impact
    global Jira_user_details
    global Jira_workaround_description
    global Jira_workaround_owner
    global Jira_epic
    global Jira_work_type
    global PULSE_csg
    global PULSE_items
    global PULSE_eventID
    global PULSE_eventURL
    global PULSE_ldap_url
    global PULSE_issueID
    global PULSE_description
    global PULSE_shortdesc
    global PULSE_eventProduct
    global PULSE_custfieldid
    global Error_Integration
    global PULSE_formID

    # Get PULSE Incident data by SQL query

    try:
        mylog.info("--> Collect PULSE event data and target Jira project")
        connection = pypyodbc.connect('Driver={SQL Server};'
                                     'Server=' + PULSE_SQL_server +';'
                                     'Database='+ PULSE_SQL_database +';'
                                     'uid=' + PULSE_SQL_uid + ';pwd=' + PULSE_SQL_pwd)

        cursor = connection.cursor()
        SQLCommand = ("select i.incident_id, i.incident_ref,i.aff_usr_name as 'Affected User', ISNULL(i.short_desc, '') short_desc,id.remarks as 'Description',it.item_sc,it.item_n, ic.inc_cat_n, i.serv_dept_id, REPLACE(CONVERT(NVARCHAR, date_logged, 111), '/', '-'), pd.product_sc, pd.product_n, cusr.usr_n, ISNULL(sd.sectn_sc, '') sectn_sc, ISNULL(sd.dept_sc, '') dept_sc, ISNULL(br.bldng_n, '') bldng_n, ISNULL(br.room_n, '') room_n, ISNULL(cusr.tele, '') tele, CASE WHEN ISNULL(rep_usr_name,'--')='--' or rep_usr_name = '' THEN '--' ELSE rep_usr_name END as rep_usr_name, ISNULL(i.csg_sc, '') csg_sc, id.ent_id from incident i, item it, inc_cat ic, inc_data id, product pd, assyst_usr ausr, usr cusr, sectn_dept sd, bldng_room br where  it.item_id = i.item_id and i.inc_cat_id = ic.inc_cat_id and it.product_id = pd.product_id and i.ass_usr_id = ausr.assyst_usr_id and i.incident_id = '" + PULSE_eventID + "' and id.incident_id = '" + PULSE_eventID + "' and (id.event_type = 'i' or id.event_type = 'p') and i.aff_usr_id=cusr.usr_id and cusr.sectn_dept_id=sd.sectn_dept_id and cusr.bldng_room_id=br.bldng_room_id")

        cursor.execute(SQLCommand)
        results = cursor.fetchone()

        # reporter format in JIRA is Firstname.Lastname, has to convert the name
        # assyst converts Plain Text to Formatted Text, Returns and Line feeds are transformed into <br /> tags, so CRLF, LF and CR all become <br />

        PULSE_eventID = str(results[0])
        #PULSE_description = str(results[4]).replace("<br />","\n")
        if str(results[4]).find('<==# ADDITIONAL INFORMATION')!= -1:
            PULSE_description = str(results[4])[0:str(results[4]).find('<==# ADDITIONAL INFORMATION')]
        else:
            PULSE_description = str(results[4])
        #PULSE_description = str(results[4])
        PULSE_eventProduct = str(results[10])
        PULSE_items.append(results[5])
        PULSE_csg = results[19]
        PULSE_formID = results[20]

        if PULSE_csg =='INNOVAPOST':
            PULSE_ldap_url = PULSE_ldap_inn_url
            PULSE_eventURL = "[" + PULSE_eventREF + "|" + PULSE_ldap_inn_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]"
        else:
            if PULSE_csg =='CANADA POST':
                PULSE_ldap_url = PULSE_ldap_cpc_url
                PULSE_eventURL = "[" + PULSE_eventREF + "|" + PULSE_ldap_inn_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" + \
                                 "(CPC user - Please click this "+ "[" + "link" + " | " + PULSE_ldap_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" + ")\n"
            else:
                if PULSE_csg =='PUROLATOR':
                    PULSE_ldap_url = PULSE_ldap_pur_url
                    PULSE_eventURL = "[" + PULSE_eventREF + "|" + PULSE_ldap_inn_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" + \
                                     "(Purolator user - Please click this " + "[" + "link" + " | " + PULSE_ldap_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" + ")\n"
                else:
                    if PULSE_csg =='SCI':
                        PULSE_ldap_url = PULSE_ldap_sci_url
                        PULSE_eventURL = "[" + PULSE_eventREF + "|" + PULSE_ldap_inn_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" + \
                                         "(SCI user - Please click this " +"[" + "link" + " | " + PULSE_ldap_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]" +")\n"
                    else:
                        PULSE_ldap_url = PULSE_ldap_inn_url
                        PULSE_eventURL = "[" + PULSE_eventREF + "|" + PULSE_ldap_inn_url + "/assystweb/application.do#event%2FDisplayEvent.do%3Fdispatch%3DgetEvent%26eventId%3D" + PULSE_eventID + "]"
        if PULSE_eventREF == PULSE_eventID :
            Jira_summary = "Problem P" + str(PULSE_eventID) + ": "+ results[3]
        else:
            Jira_summary = "Incident " + str(PULSE_eventID)[1:len(str(PULSE_eventID))] + ": " + results[3]

        PULSE_shortdesc = results[3]
        Jira_component.append({"name":results[5]})
        Jira_datelogged = results[9]
        Jira_business_contact = results[18]
        Jira_user_details ="Affected User: " + results[2] + "\n" + \
                           "Section-Department: " + results[13]+"-"+results[14] + "\n" +\
                           "Building-Room: " + results[15]+"-"+results[16] + "\n" + \
                           "Telephone: " + results[17] + "\n"

        Jira_description = "*+PULSE Incident Information:+* \n" + \
                           "*Event URL:* " + PULSE_eventURL + "\n" + \
                           "*Description:* \n"+ PULSE_description+"\n"


        # Get JIRA Project ID from PULSE Product and JIRA Project mapping file

        mylog.info("    Get Jira Project")
        JiraProjects = GetJiraProjects()
        if JiraProjects != {}:
            Jira_projectID = JiraProjects.get(PULSE_eventProduct.replace(" ",""))
            Jira_projectName = JiraProjects.get(PULSE_eventProduct.replace(" ",""))
            Jira_epic = JiraProjects.get(PULSE_eventProduct.replace(" ", ""))
            if Jira_projectID == None and Jira_projectName == None :
                Error_Integration = "No project has been mapped for this PULSE product shortcode. Please open a PULSE Service Request to enable Jira integration. Link to Jira Setup Service Request: " + PULSE_ldap_url + "/assystnet/#serviceOfferings/1530"
                return 0
            else:
                Jira_projectID = Jira_projectID.split('#')[0]
                Jira_projectName = Jira_projectName.split('#')[1]
                Jira_epic = Jira_epic.split('#')[2]

        else:
            Error_Integration = "Project mapping file is either empty or not available"
            mylog.error("No project mapping. Please contact PULSE team to enable PULSE-Jira Integration")
            return 0

        # Get Incident/Problem form ID to locate customfield id - Jira Issue Id field

        SQLCommand = ("select ancestor_ent_id, jptsys_web_cust_prop_id from jptsys_web_cust_prop where jptsys_web_cust_prop_sc='JIRA ISSUE ID'")
        cursor.execute(SQLCommand)
        results = cursor.fetchall()

        if PULSE_formID in [row[0] for row in results]:
            index = [row[0] for row in results].index(PULSE_formID)
            PULSE_custfieldid = [row[1] for row in results][index]
        else:
            PULSE_custfieldid = ""

        # Get PULSE Incident custom fields data by SQL query. 8484-business priority, 8485-business impact, 8486-preferred resolution date
        # 8487-business grouping, 8488-resulotion type, 9312-workaround description, 9313-workaround owner, 9486 (i) or 9766 (p) -PULSE issue ID

        mylog.info("    Collect PULSE event custom field data")
        Jira_work_type = []
        if PULSE_eventREF == PULSE_eventID :
            # SQL for Problem form
            Jira_work_type.append({"value": "Problem"})
            SQLCommand = ("select cust3.jptsys_web_cust_prop_id, ISNULL(Cast(date_val as varchar(25)),'--') date_val,CASE WHEN ISNULL(string_value,'') = '' or string_value = '' THEN '--' ELSE string_value END as string_value, ISNULL(jptsys_web_lkup_data_n, '--')  jptsys_web_lkup_data_n, cust2.jptsys_web_cust_prop_n from jptsys_web_cust_prop cust2, jptsys_web_cust_prop_cont cust3 ,jptsys_web_lkup_data cust1 where entity_id='" + PULSE_eventID + "' and cust3.jptsys_web_cust_prop_id in ('8191','8192','8193','8489','8490','9314','9315'," + str(PULSE_custfieldid) + ") and (single_sel_val_id=cust1.jptsys_web_lkup_data_id) and (cust2.jptsys_web_cust_prop_id = cust3.jptsys_web_cust_prop_id) order by CASE cust2.jptsys_web_cust_prop_n WHEN 'Business Planning Priority' THEN 1 WHEN 'Business Impact' THEN 2 WHEN 'Preferred Resolution Date' THEN 3 WHEN 'Implementation Grouping' THEN 4 WHEN 'Resolution Type' THEN 5 WHEN 'Workaround Description' THEN 6 WHEN 'Workaround Business Owner' THEN 7 WHEN 'Jira Issue ID' THEN 8 END")
        else:
            # SQL for incident (standard form) from support portal
            Jira_work_type.append({"value": "Incident"})
            SQLCommand = ("select cust3.jptsys_web_cust_prop_id, ISNULL(Cast(date_val as varchar(25)),'--') date_val,CASE WHEN ISNULL(string_value,'') = '' or string_value = '' THEN '--' ELSE string_value END as string_value, ISNULL(jptsys_web_lkup_data_n, '--')  jptsys_web_lkup_data_n, cust2.jptsys_web_cust_prop_n from jptsys_web_cust_prop cust2, jptsys_web_cust_prop_cont cust3 ,jptsys_web_lkup_data cust1 where entity_id='" + PULSE_eventID + "' and cust3.jptsys_web_cust_prop_id in ('8484','8485','8486','8487','8488','9312','9313'," + str(PULSE_custfieldid) + ") and (single_sel_val_id=cust1.jptsys_web_lkup_data_id) and (cust2.jptsys_web_cust_prop_id = cust3.jptsys_web_cust_prop_id) order by CASE cust2.jptsys_web_cust_prop_n WHEN 'Business Planning Priority' THEN 1 WHEN 'Business Impact' THEN 2 WHEN 'Preferred Resolution Date' THEN 3 WHEN 'Implementation Grouping' THEN 4 WHEN 'Resolution Type' THEN 5 WHEN 'Workaround Description' THEN 6 WHEN 'Workaround Business Owner' THEN 7 WHEN 'Jira Issue ID' THEN 8 END")
            cursor.execute(SQLCommand)
            results = cursor.fetchall()

            # Check to see if the incident (form: APPLICATION ISSUE) is from self-serve portal
            test = 9486 in [row[0] for row in results]
            if len(results)==0 or not(9486 in [row[0] for row in results]):
                SQLCommand = ("select cust3.jptsys_web_cust_prop_id, ISNULL(Cast(date_val as varchar(25)),'--') date_val,CASE WHEN ISNULL(string_value,'') = '' or string_value = '' THEN '--' ELSE string_value END as string_value, ISNULL(jptsys_web_lkup_data_n, '--')  jptsys_web_lkup_data_n, cust2.jptsys_web_cust_prop_n from jptsys_web_cust_prop cust2, jptsys_web_cust_prop_cont cust3 ,jptsys_web_lkup_data cust1 where entity_id='" + PULSE_eventID + "' and cust3.jptsys_web_cust_prop_id in ('9977','9978','9979','9980','9981','9982','9983'," + str(PULSE_custfieldid) + ") and (single_sel_val_id=cust1.jptsys_web_lkup_data_id) and (cust2.jptsys_web_cust_prop_id = cust3.jptsys_web_cust_prop_id) order by CASE cust2.jptsys_web_cust_prop_n WHEN 'Business Planning Priority' THEN 1 WHEN 'Business Impact' THEN 2 WHEN 'Preferred Resolution Date' THEN 3 WHEN 'Implementation Grouping' THEN 4 WHEN 'Resolution Type' THEN 5 WHEN 'Workaround Description' THEN 6 WHEN 'Workaround Business Owner' THEN 7 WHEN 'Jira Issue ID' THEN 8 END")

        cursor.execute(SQLCommand)
        results = cursor.fetchall()

        custfieldIds = [row[0] for row in results]
        if (8484 in custfieldIds) or (9977 in custfieldIds) or (8191 in custfieldIds):
            if results[0][3] != "--":
                Jira_business_priority = int(results[0][3][0])
            else:
                Jira_business_priority = 0
        else:
            Jira_business_priority = 0

        # get customfield ID for Business impact field

        if not(8485 in custfieldIds) and not(9978 in custfieldIds) and not(8192 in custfieldIds):
            Jira_business_impact = "--"
        else:
            if 8485 in custfieldIds:
                index = custfieldIds.index(8485)
            elif 9978 in custfieldIds:
                index = custfieldIds.index(9978)
            else:
                index = custfieldIds.index(8192)
            Jira_business_impact = results[index][2]

        # get customfield ID for Workaround description field

        if not(9312 in custfieldIds) and not(9982 in custfieldIds) and not(9314 in custfieldIds):
            Jira_workaround_description = "--"
        else:
            if 9312 in custfieldIds:
                index = custfieldIds.index(9312)
            elif 9982 in custfieldIds:
                index = custfieldIds.index(9982)
            else:
                index = custfieldIds.index(9314)
            Jira_workaround_description = results[index][2]

        # get customfield ID for Workaround owner field

        if not(9313 in custfieldIds) and not(9983 in custfieldIds) and not(9315 in custfieldIds):
            Jira_workaround_owner = "--"
        else:
            if 9313 in custfieldIds:
                index = custfieldIds.index(9313)
            elif 9983 in custfieldIds:
                index = custfieldIds.index(9983)
            else:
                index = custfieldIds.index(9315)
            Jira_workaround_owner = results[index][2]

        # get customfield ID for Jira Issue ID field

        if not(9486 in custfieldIds) and not(9976 in custfieldIds) and not(9766 in custfieldIds):
            PULSE_issueID = "--"
        else:
            if 9486 in custfieldIds:
                index = custfieldIds.index(9486)
            elif 9976 in custfieldIds:
                index = custfieldIds.index(9976)
            else:
                index = custfieldIds.index(9766)
            PULSE_issueID = results[index][2]

        if PULSE_issueID != "--":
            Jira_issueID = PULSE_issueID

        # Get PULSE items then create Jira project component if not defined in Jira project

        mylog.info("    Get PULSE event item and linked items")
        SQLCommand = ("select it.item_sc from incident i inner join inc_data id on  id.incident_id = i.incident_id left join link_itm_grp lig on lig.incident_id = i.incident_id left join link_itm li on li.link_itm_grp_id = lig.link_itm_grp_id left join  item it on it.item_id = li.item_id inner join item MIt on MIt.item_id = i.item_id where  i.incident_id='"+ PULSE_eventID +"' and  (id.event_type = 'i' or id.event_type = 'p' ) and id.sub_event_type = 'n'")
        cursor.execute(SQLCommand)
        results = cursor.fetchall()

        if results[0][0] != None:
            for item in results:
                PULSE_items.append(item[0])
                Jira_component.append({"name":item[0]})

        connection.close()
        return 1
    except pypyodbc.Error as ex:
        Error_Integration = "SQL query failure. Failed to access PULSE incident data"
        mylog.error("SQL Error:" + Error_Integration)
        connection.close()
        return 0

# Get PULSE event attachments then add the URL links to JIRA description field

def Get_PULSE_Event_Attachment ():

    global Jira_description

    mylog.info("    Get PULSE event attachments")
    url = PULSE_url + "/assystREST/v2/events/"+ PULSE_eventID +"/attachments"
    headers = {
       "Accept": "application/json",
       "Content-Type": "application/json",
       "Authorization": PULSE_basic_auth
    }
    response = requests.request(
       "GET",
       url,
       headers=headers
    )
    JsonToPython = json.loads(response.text)

    desc_attachment = ""
    for attachment in JsonToPython:
        if PULSE_csg == "INNOVAPOST":
            desc_attachment = desc_attachment + "["+ attachment['name'] +" | " + PULSE_ldap_url + "/assystREST/v2/events/"+ PULSE_eventID + "/attachments/" +str(attachment['id']) +"/binary]" +"\n"
        else:
            if PULSE_csg == "CANADA POST":
                desc_attachment = desc_attachment + "[" + attachment['name'] + " | " + PULSE_ldap_inn_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + \
                                  "(CPC user - Please click this " + "[" + "link" + " | " + PULSE_ldap_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + ")\n"
            else:
                if PULSE_csg == "PUROLATOR":
                    desc_attachment = desc_attachment + "[" + attachment['name'] + " | " + PULSE_ldap_inn_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + \
                                      "(Purolator user - Please click this " + "[" + "link" + " | " + PULSE_ldap_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + ")\n"
                else:
                    if PULSE_csg == "SCI":
                        desc_attachment = desc_attachment + "[" + attachment['name'] + " | " + PULSE_ldap_inn_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + \
                                          "(SCI user - Please click this " + "[" + "link" + " | " + PULSE_ldap_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + ")\n"
                    else:
                        desc_attachment = desc_attachment + "[" + attachment['name'] + " | " + PULSE_ldap_url + "/assystREST/v2/events/" + PULSE_eventID + "/attachments/" + str(attachment['id']) + "/binary]" + "\n"

    if desc_attachment != "":
        Jira_description = "*+PULSE Event Attached Files:+* \n" + desc_attachment + "\n\n" + Jira_description


# Prepare JIRA Payload then create JIRA issue for Incident

def Check_Component ():

    global PULSE_items
    Project_Components = []

    url = Jira_url + "/rest/api/2/project/" + Jira_projectID + "/components"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": Jira_basic_auth
    }
    response = requests.request(
        "GET",
        url,
        headers=headers
    )
    JsonToPython = json.loads(response.text)

    for component in JsonToPython:
        if component['name'] != "":
            Project_Components.append(component['name'])

    for x in range(0,len(PULSE_items)):
        if PULSE_items[x] not in Project_Components:
            Create_Component(PULSE_items[x])

    #***** 05-15-2020 added by Albert Hsieh - Julia Cronin's requirement
    if "Run" not in Project_Components:
        Create_Component("Run")

    Jira_component.append({"name": "Run"})

def Create_Component (item):

    global Jira_url
    global Jira_projectName

    CompURL = Jira_url + "/rest/api/2/component"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": Jira_basic_auth
    }

    payload = json.dumps(
        {
            "name": item,
            "description": "",
            "leadUserName": "",
            "assigneeType": "UNASSIGNED",
            "isAssigneeTypeValid": False,
            "project": Jira_projectName
        }
    )

    response = requests.request(
        "POST",
        CompURL,
        data=payload,
        headers=headers
    )


def Create_JIRA_Issue ():

    global Jira_issueID
    global Jira_issueURL
    global Error_Integration
    global Jira_component
    global PULSE_eventREF
    global PULSE_shortdesc

    try:
        mylog.info("--> Create Jira story/issue")
        Check_Component()
        url = Jira_url + "/rest/api/latest/issue"

        headers = {
           "Accept": "application/json",
           "Content-Type": "application/json",
           "Authorization": Jira_basic_auth
        }

        if Jira_epic.lower() != "none" :
            payload = json.dumps( {
              "update": {

              },
              "fields": {
                "project": {
                      "id": Jira_projectID
                  },
                  "issuetype": {
                      "name": "Story"
                  },
                #"assignee": {
                  #"name": "albert.hsieh"
                  #"name": Jira_assignee
                #},
                #"reporter": {
                #    "name": ""
                #},
                "priority": {
                  #"name": Jira_priority
                    "name": "Low"
                },
                "description": Jira_description,

                "summary": Jira_summary,

                # Epic Link (customfield not system field) accept only Epic Link ID not Epic Link name
                "customfield_10008": Jira_epic,

                #"components": [{"name":"PULSE"},{"name":"DIA APPLICATION"},{"name":"EPOST REPORTING"},{"name":"DIA - ODS"},],

                # Labels accept only arraylist because it allows multiple values, no space is allowed for Label
                #"labels": [  "PULSE","Manual"],
                #"labels": [Jira_component],

                # Epic Link (customfield not system field) accept only Epic Link ID not Epic Link name
                #"customfield_10008":"TEST2PJI-24",

                # Sprint (customfield not system field) accept only number value, have to find out the sprint number
                #"customfield_10007":1251,

                # ----------------

                # Start date (customfield not system field) mapped to PULSE Date/Time logged, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss.sTZD
                "customfield_12820": Jira_datelogged,

                # Workaround (customfield not system field) mapped to PULSE Workaround Description
                #"customfield_13900": Jira_workaround_description, # Jira sandbox
                "customfield_14400": Jira_workaround_description,

                # Workaround Business Owner (customfield not system field) mapped to PULSE Workaround Business Owner
                #"customfield_13901": Jira_workaround_owner, # Jira sandbox
                "customfield_14401": Jira_workaround_owner,

                #PULSE # (customfield not system field) accept simple text? mapped to PULSE REF
                "customfield_11104": PULSE_eventREF,

                # User details (customfield not system field) mapped to PULSE affected user details
                "customfield_13013": Jira_user_details,

                # Change Mgmt Required (customfield not system field). Yes/No drop down field
                #"customfield_13902": { "value": "No"}, # Jira sandbox
                "customfield_14402": {"value": "No"},

                # Business Contact (customfield not system field). mapped to PULSE reporting user
                "customfield_12213": Jira_business_contact,

                # Business Impact (customfield not system field). mapped to PULSE Business Impact
                #"customfield_13903": Jira_business_impact, # Jira sandbox
                "customfield_14403": Jira_business_impact,

                # Business Value (customfield not system field). Type number, mapped to PULSE Business Planning Priority
                "customfield_10005": Jira_business_priority,

                # Components (system field). mapped to PULSE itemA and linked items
                "components": Jira_component,

                # Story Point (system field)
                "customfield_10004": 1,

                # Work Type (system field). array type with value Incident, Service Request, Problem, Nonw, General Inquiry
                "customfield_15301": Jira_work_type
              }
            } )
        else:
            payload = json.dumps({
                "update": {

                },
                "fields": {
                    "project": {
                        "id": Jira_projectID
                    },
                    "issuetype": {
                        "name": "Story"
                    },
                    # "assignee": {
                    # "name": "albert.hsieh"
                    # "name": Jira_assignee
                    # },
                    # "reporter": {
                    #    "name": ""
                    # },
                    "priority": {
                        # "name": Jira_priority
                        "name": "Low"
                    },
                    "description": Jira_description,

                    "summary": Jira_summary,

                    # "components": [{"name":"PULSE"},{"name":"DIA APPLICATION"},{"name":"EPOST REPORTING"},{"name":"DIA - ODS"},],

                    # Labels accept only arraylist because it allows multiple values, no space is allowed for Label
                    # "labels": [  "PULSE","Manual"],
                    # "labels": [Jira_component],

                    # Epic Link (customfield not system field) accept only Epic Link ID not Epic Link name
                    # "customfield_10008":"TEST2PJI-24",

                    # Sprint (customfield not system field) accept only number value, have to find out the sprint number
                    # "customfield_10007":1251,

                    # ----------------

                    # Start date (customfield not system field) mapped to PULSE Date/Time logged, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss.sTZD
                    "customfield_12820": Jira_datelogged,

                    # Workaround (customfield not system field) mapped to PULSE Workaround Description
                    # "customfield_13900": Jira_workaround_description, # Jira sandbox
                    "customfield_14400": Jira_workaround_description,

                    # Workaround Business Owner (customfield not system field) mapped to PULSE Workaround Business Owner
                    # "customfield_13901": Jira_workaround_owner, # Jira sandbox
                    "customfield_14401": Jira_workaround_owner,

                    # PULSE # (customfield not system field) accept simple text? mapped to PULSE REF
                    "customfield_11104": PULSE_eventREF,

                    # User details (customfield not system field) mapped to PULSE affected user details
                    "customfield_13013": Jira_user_details,

                    # Change Mgmt Required (customfield not system field). Yes/No drop down field
                    # "customfield_13902": { "value": "No"}, # Jira sandbox
                    "customfield_14402": {"value": "No"},

                    # Business Contact (customfield not system field). mapped to PULSE reporting user
                    "customfield_12213": Jira_business_contact,

                    # Business Impact (customfield not system field). mapped to PULSE Business Impact
                    # "customfield_13903": Jira_business_impact, # Jira sandbox
                    "customfield_14403": Jira_business_impact,

                    # Business Value (customfield not system field). Type number, mapped to PULSE Business Planning Priority
                    "customfield_10005": Jira_business_priority,

                    # Components (system field). mapped to PULSE itemA and linked items
                    "components": Jira_component,

                    # Story Point (system field)
                    "customfield_10004": 1,

                    # Work Type (system field). array type with value Incident, Service Request, Problem, Nonw, General Inquiry
                    "customfield_15301": Jira_work_type
                }
            })

        response = requests.request(
           "POST",
           url,
           data=payload,
           headers=headers
        )
        response.raise_for_status()  # throw exception if request does not retun 2xx

        # http status is 2xx, issue must have been created
        JsonToPython = json.loads(response.text)
        Jira_issueID = JsonToPython['key']
        PULSE_shortdesc = PULSE_shortdesc + " (Jira Story: " + Jira_issueID + ")"
        mylog.info("    Corresponding Jira issue was created: " + Jira_issueID + "(Incident/Problem " + PULSE_eventREF +")")

    except requests.exceptions.HTTPError as e:
        json_response = json.loads(response.text)
        Error_Integration = "JIRA issue "+ list(json_response['errors'].keys())[0] + "--" + list(json_response['errors'].values())[0] + " Please retake the action after resolving the problem."
        mylog.error("Event " + PULSE_eventREF + "," + Error_Integration)

# Retrieve from JIRA RestAPI response the issue ID and prepare PULSE Payload to update incident

def Update_JIRA_ID ():

    global PULSE_description

    try:

        # Update the JIRA issue ID or error message (SQL Query, JIRA Issue Creation) back to PULSE event description field

        if PULSE_issueID == "--":
            mylog.info("--> Update Jira story/issue ID back to PULSE")

            PULSEurl = PULSE_url + "/assystREST/v2/events/" + PULSE_eventID

            headers = {
                "Accept": "application/Json",
                "Content-Type": "application/Json",
                "Authorization": PULSE_basic_auth
            }
            if Error_Integration == "":
                PULSE_desc = "Corresponding Jira issue ID: " + Jira_issueID + " ( "+Jira_url+"/browse/"+Jira_issueID+" )"+"\n\n" + PULSE_description +"\n\n"

                # Incident/Problem - Jira Issue Field ID and Summary Field

                if PULSE_custfieldid != "":

                    #PULSE incident/problem with Jira Issue ID customfield
                    pulsePayload = json.dumps( {
                       #"remarks": PULSE_desc,
                       "shortDescription": PULSE_shortdesc,
                       "customFields": {
                         "customFieldId": PULSE_custfieldid,
                         "customFieldType": "1",
                         "stringValue": Jira_issueID
                       }
                    })
                else:
                    # PULSE incident/problem without Jira Issue ID customfield
                    pulsePayload = json.dumps( {
                       #"remarks": PULSE_desc,
                       "shortDescription": PULSE_shortdesc,
                    })

                response = requests.request(
                   "POST",
                   PULSEurl,
                   data=pulsePayload,
                   headers=headers
                )

                # Take JIRA STORY CREATED action to add story URL

                PULSEacturl1 = PULSE_url + "/assystREST/v2/actions"

                headers1 = {
                    "Accept": "application/xml",
                    "Content-Type": "application/xml",
                    "Authorization": PULSE_basic_auth
                }

                pulseActPayload1 = "<action><eventId>" + PULSE_eventID + "</eventId><remarks>" + Jira_url+"browse/"+Jira_issueID + "</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>JIRA STORY CREATED</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept> </action>"

                response = requests.request(
                    "POST",
                    PULSEacturl1,
                    data=pulseActPayload1,
                    headers=headers1
                )

            else:
                PULSE_errshortdesc = "Export to Jira error: " + Error_Integration

                # Take JIRA ERROR action when error exporting to Jira

                PULSEacturl1 = PULSE_url + "/assystREST/v2/actions"

                headers1 = {
                    "Accept": "application/xml",
                    "Content-Type": "application/xml",
                    "Authorization": PULSE_basic_auth
                }

                pulseActPayload1 = "<action><eventId>" + PULSE_eventID + "</eventId><remarks>" + PULSE_errshortdesc + "</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>JIRA ERROR</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept> </action>"

                response = requests.request(
                    "POST",
                    PULSEacturl1,
                    data=pulseActPayload1,
                    headers=headers1
                )

            response.raise_for_status()  # throw exception if request does not retun 2xx

        else:
            # Jira story already exists (Jira Issue ID field), taking Jira Error action
            PULSE_errshortdesc = "Export to Jira error: Jira story already exists (Jira issue Id field is not empty)"

            PULSEacturl1 = PULSE_url + "/assystREST/v2/actions"

            headers1 = {
                "Accept": "application/xml",
                "Content-Type": "application/xml",
                "Authorization": PULSE_basic_auth
            }

            pulseActPayload1 = "<action><eventId>" + PULSE_eventID + "</eventId><remarks>" + PULSE_errshortdesc + "</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>JIRA ERROR</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept> </action>"

            response = requests.request(
                "POST",
                PULSEacturl1,
                data=pulseActPayload1,
                headers=headers1
            )
            response.raise_for_status()  # throw exception if request does not retun 2xx

        if Close_Event_Flag == "close":

            # Take a Close To Pending (Pending-Closure) action to close the event if issue is successfully created

            mylog.info("--> Close PULSE Event")

            if Error_Integration == "" and Jira_issueID != "--" and PULSE_issueID != Jira_issueID:
                PULSEacturl = PULSE_url + "/assystREST/v2/actions"

                print (PULSEacturl)

                headers = {
                   "Accept": "application/xml",
                   "Content-Type": "application/xml",
                   "Authorization": PULSE_basic_auth
                }

                actionDesc = "Jira story created successfully (Jira Story ID:" + Jira_issueID + ") - Closing Incident/Problem"

                pulseActPayload = "<action><eventId>"+PULSE_eventID+"</eventId><remarks>JIRA issue created successfully. Closing Incident.</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>PENDING-CLOSURE</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept>  <causeCategory><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>TASK/REQ COMP</parameterValue></resolvingParameters></causeCategory> </action>"
                #pulseActPayload = "<action><eventId>" + PULSE_eventID + "</eventId><remarks>"+ actionDesc + "</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>PENDING-CLOSURE</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept>  <causeCategory><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>CLOSE TO JIRA</parameterValue></resolvingParameters></causeCategory> </action>"

                response = requests.request(
                   "POST",
                   PULSEacturl,
                   data=pulseActPayload,
                   headers=headers
                )
                response.raise_for_status()  # throw exception if request does not retun 2xx
                # print (response.text)

    except requests.exceptions.HTTPError as e:
        print(response.text)
        mylog.error("Event " + PULSE_eventID + ", HTTP exception - Error updating JIRA issue ID back to PULSE")

def start():
    mylog.info("** Start PULSE and JIRA Integration (" + PULSE_eventREF +")")
    Get_Config_Info()
    if Get_PULSE_Event_Data() == 0 :
        Update_JIRA_ID()
        sys.exit()
    else:
        if PULSE_issueID =="--":
            Get_PULSE_Event_Attachment()
            Create_JIRA_Issue()
        Update_JIRA_ID()


if __name__ == '__main__' :
    if len(sys.argv)== 4:
        PULSE_eventID = sys.argv[1]
        PULSE_eventREF = sys.argv[2]
        Close_Event_Flag = sys.argv[3]
        start()
    else:
        mylog.error("Wrong argument number")
        sys.exit()