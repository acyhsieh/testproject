import imaplib
import email
import ssl
import html2text
import requests
import json
import configparser
import regex
from datetime import datetime
from datetime import timedelta
from myLog import MyLog
#from requests.adapters import HTTPAdapter
#from requests.packages.urllib3.poolmanager import PoolManager

mail_id = ""                # mailbox login id
mail_pwd = ""               # mailbox login pwd
mail_folder = ""            # mailbox folder tp hold target notification to be processed
ImapServer = ""             # CPG imapserver
updatefieldlist = []        # updated date and field list of the whole history (changelog)
updatetype = 0              # 0 for all the field updated, 1 for assign user, 2 for comment update
target_field = ""           # specific taregt field name from notification
target_field_history = ""   # specific target filed name in history
target_field_list = []      # field name list from Jira notification - one notification can have multiple updates
target_field_list_notification = []     # fields in scope - notification
target_field_list_history = []          # fields' name in scope - changelog
Jira_story_key = ""         # Jira story key
Jira_update = ""            # final formated story update string
Jira_url = ""               # URL to trigger Jira rest API
Jira_basic_auth = "Basic "
PULSE_url = ""              # URL to triger PULSE rest API
PULSE_basic_auth = "Basic "

# Initialize Python Logging Feature

mylog=MyLog()

#class MyAdapter(HTTPAdapter):
#    def init_poolmanager(self, connections, maxsize, block=False):
#        self.poolmanager = PoolManager(num_pools=connections,
#                                       maxsize=maxsize,
#                                       block=block,
#                                       ssl_version=ssl.PROTOCOL_TLSv1_2)

def Get_Config_Info ():

    global mail_id
    global mail_pwd
    global mail_folder
    global ImapServer
    global PULSE_url
    global PULSE_basic_auth
    global Jira_url
    global Jira_basic_auth
    global target_field_list_notification
    global target_field_list_history

    try:
        mylog.info("--> Get program configuration file")
        #with open("../PULSE_JIRA_Integration/"+"PULSE_JIRA_Config.txt") as fp:
        with open("Jira_Update_Config.txt") as fp:
            config = configparser.ConfigParser()
            # config.readfp(fp)
            config.read_file(fp)
    except configparser.Error:
        mylog.error("Error: Failed to retrieve configuration file. Please check the path and name of the file")

    mail_id = config.get('MAIL', 'Mail_ID')
    mail_pwd = config.get('MAIL', 'Mail_PWD')
    mail_folder = config.get('MAIL', 'Mail_Folder')
    ImapServer =  config.get('MAIL', 'IMAPServer')
    PULSE_url = config.get('PULSE', 'URL')
    PULSE_basic_auth = PULSE_basic_auth + config.get('PULSE', 'PULSE_BA')   # zzeng(FDDWD756756snsns) or Albert.Hsieh or *zzeng (PULSE4LIFE2014??)
    Jira_url = config.get('JIRA', 'URL')
    Jira_basic_auth = Jira_basic_auth + config.get('JIRA', 'Jira_BA')       # *1043-SVC-PULSEAD-PRD or Albert.Hsieh
    target_field_list_notification = config.get('JIRA', 'Target_field_list_notification').split(',')
    target_field_list_history = config.get('JIRA', 'Target_field_list_history').split(',')

def Get_Update_Timestamp (imaptimestamp):
    timestamp1 = imaptimestamp[imaptimestamp.find(',')+1:]
    timestamp2 = timestamp1[:timestamp1.find('-')-1].strip()
    if int(timestamp2.split()[0]) < 10 :
        timestamp3 = "0" + timestamp2
    else:
        timestamp3=timestamp2
    return timestamp3

def find_all_indexes(input_str, search_str):
    strIndex = []
    length = len(input_str)
    index = 0
    while index < length:
        i = input_str.find(search_str, index)
        if i == -1:
            return strIndex
        strIndex.append(i)
        index = i + 1
    return strIndex

def PULSE_Jira_Update (message):

    try:
        if message != "":
            # Get PULSE event ID from Jira field PULSE# (customfield_11104)
            url = Jira_url + "/rest/api/latest/issue/" + Jira_story_key + "?fields=customfield_11104"

            headers = {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": Jira_basic_auth
            }
            response = requests.request(
                "GET",
                url,
                headers=headers
            )
            response.raise_for_status()  # throw exception if request does not retun 2xx

            JsonToPython = json.loads(response.text)
            fields = JsonToPython['fields']
            PULSE_eventID = fields['customfield_11104']

            if PULSE_eventID!=None:
                if PULSE_eventID[:1] == 'S':
                    PULSE_eventID = "5" + PULSE_eventID[1:]
                else:
                    if len(PULSE_eventID) >= 7:
                        PULSE_eventID = "1" + PULSE_eventID
            else:
                PULSE_eventID = ""

            if PULSE_eventID != "":

                # Update message back to PULSE action Jira Update

                PULSEacturl = PULSE_url + "/assystREST/v2/actions"

                headers = {
                    "Accept": "application/xml",
                    "Content-Type": "application/xml",
                    "Authorization": PULSE_basic_auth
                }

                pulseActPayload = "<action><eventId>" + PULSE_eventID + "</eventId><remarks>" + message + "</remarks><actionType><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>JIRA UPDATE</parameterValue></resolvingParameters></actionType><actionedBy><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>ZZENG</parameterValue></resolvingParameters></actionedBy><actioningServDept><resolvingParameters><parameterName>shortCode</parameterName><parameterValue>SERVICE ACCOUNT</parameterValue></resolvingParameters></actioningServDept> </action>"

                # below three lines are for TLS 1.2
                #s = requests.Session()
                #s.mount('https://', MyAdapter())
                #res = s.post(PULSEacturl, headers=headers, data=pulseActPayload)

                response = requests.request(
                    "POST",
                    PULSEacturl,
                    data=pulseActPayload,
                    headers=headers
                )
                response.raise_for_status()  # throw exception if request does not retun 2xx
                # print (response.text)
                mylog.info("--> Take Jira Update Action (" + PULSE_eventID + "): \n" + message)
            else:
                mylog.error("Error: PULSE event id is empty")

    except requests.exceptions.HTTPError as e:
        print(response.text)
        mylog.error("Event " + PULSE_eventID + ", HTTP exception - Error updating JIRA story ID back to PULSE")

# force https to use protocol TLS 1.2

# s = requests.Session()
# s.mount('https://', MyAdapter())

Get_Config_Info()

mylog.info("--> Access Mailbox using IMAP")

mail = imaplib.IMAP4_SSL(ImapServer)
mail.login(mail_id, mail_pwd)

mail.select(mail_folder, False)  # connect to inbox.
result, data = mail.uid('SEARCH', None, '(UNSEEN)')
ids = data[0].decode('UTF-8','ignore') # data is a list.
id_list = ids.split() # ids is a space separated string
for targetemail in id_list:
    result, emailbody = mail.uid('fetch', targetemail, '(RFC822)')  # fetch the email body for the given ID
    bodytext = emailbody[0][1]
    # continue inside the same for loop as above
    raw_email_string = bodytext.decode('utf-8')
    # converts byte literal to string removing b''
    email_message = email.message_from_string(raw_email_string)

    if email_message.is_multipart():
        for payload in email_message.get_payload():
            print('To:\t\t', email_message['To'])
            print('From:\t', email_message['From'])
            print('Subject:', email_message['Subject'])
            print('Date:\t', email_message['Date'])
            mylog.info("** Start Processing CPGJira notification (" + email_message['Date'] + ")" )
            update_timestamp = Get_Update_Timestamp(email_message['Date'])

            # date on notification and date on changelog not matching - set 3 minutes period in order to identify changes

            period_seconds = timedelta(seconds=60)
            notification_timestamp = datetime.strptime(update_timestamp, '%d %b %Y %H:%M:%S')
            buttom_timestamp = datetime.strptime(update_timestamp, '%d %b %Y %H:%M:%S') - period_seconds
            for part in email_message.walk():
                Jira_update = ""
                target_field = ""
                target_field_list = []

                # format Jira email notification to string list

                if (part.get_content_type() == 'text/html') and (part.get('Content-Disposition') is None):
                    text = f"{part.get_payload(decode=True)}"
                    html = text.replace("b'", "")
                    h = html2text.HTML2Text()
                    h.ignore_links = True
                    h.ignore_images = True
                    h.hide_strikethrough = True
                    output = (h.handle(f'''{html}''').replace("\\r\\n", ""))
                    output = output.replace("'", "")
                    output = output.replace("|", "")
                    output = output.replace("---|---", "")
                    output = output.replace("---", "")
                    outputlist = output.splitlines()                    # splits the string based on the line boundary
                    outputlist = [x.strip(' ') for x in outputlist]     # remove leading and tailing spaces
                    outputlist = list(filter(None, outputlist))         # remove empty item in list
                    del outputlist[-1]                                  # remove last four unnecessary strings
                    del outputlist[-1]
                    del outputlist[-1]
                    del outputlist[-1]
                    outputlist_lowercase = [item.lower() for item in outputlist]

                    # get Jira story key from email subject of Jira update notification

                    subject_index= email_message['Subject'].find(':')
                    subject_string_list = email_message['Subject'][:subject_index].split()
                    Jira_story_key = subject_string_list[len(subject_string_list)-1]

                    # Use regular expression to locate ## x update(s) string in HTML plain text - user & date modify

                    pattern = regex.compile(r'##[\s]*[\d]*\supdate[s]*')
                    updateflag = pattern.findall(output)
                    if updateflag:
                        targetfieldindex = [i for i, s in enumerate(outputlist) if updateflag[0] in s]
                        updatepattern1 = regex.compile(r'\sAM$')
                        updatepattern2 = regex.compile(r'\sPM$')
                        updateflag1 = updatepattern1.findall(outputlist[targetfieldindex[0]+1])
                        updateflag2 = updatepattern2.findall(outputlist[targetfieldindex[0]+1])
                        if updateflag1 or updateflag2:
                            datestr = outputlist[targetfieldindex[0] + 1]
                            beginindex = outputlist[targetfieldindex[0] + 1].find('**')
                            endindex = outputlist[targetfieldindex[0] + 1].find('**', beginindex + 2) + 2
                            namemodify = outputlist[targetfieldindex[0] + 1][beginindex:endindex]
                        else:
                            datestr = outputlist[targetfieldindex[0]+1] + ' ' + outputlist[targetfieldindex[0]+2]
                            namestr = (outputlist[targetfieldindex[0] + 1] + ' ' + outputlist[targetfieldindex[0] + 2])
                            beginindex = namestr.find('**')
                            endindex = namestr.find('**', beginindex + 2) + 2
                            namemodify = namestr[beginindex:endindex]
                        dateindex = datestr.find('** on')
                        datestr = datestr[dateindex+6:]

                        # can only capture field changes within time period because the notification does not exactly match

                        top_period_seconds = timedelta(seconds=60)
                        buttom_period_seconds = timedelta(seconds=30)
                        update_notification_timestamp = datetime.strptime(update_timestamp, '%d %b %Y %H:%M:%S') + top_period_seconds
                        update_buttom_timestamp = datetime.strptime(datestr, '%d/%b/%y %I:%M %p') - buttom_period_seconds

                        i=targetfieldindex[0]+3
                        while i<len(outputlist) :
                            field_notification = outputlist[i][:outputlist[i].find(':')]
                            if field_notification in target_field_list_notification:
                                target_field_list.append(field_notification)
                            i=i+1

                    # Use regular expression to locate ## x comment(s) string in HTML plain text - user & date modify

                    pattern = regex.compile(r'##[\s]*[\d]*\scomment[s]*')
                    commentflag = pattern.findall(output)
                    if commentflag:
                        targetfieldindex = [i for i, s in enumerate(outputlist) if commentflag[0] in s]
                        commentpattern1 = regex.compile(r'\sAM$')
                        commentpattern2 = regex.compile(r'\sPM$')
                        commentflag1 = commentpattern1.findall(outputlist[targetfieldindex[0]+1])
                        commentflag2 = commentpattern2.findall(outputlist[targetfieldindex[0]+1])
                        if commentflag1 or commentflag2:
                            datestr = outputlist[targetfieldindex[0] + 1]
                            beginindex = outputlist[targetfieldindex[0] + 1].find('**')
                            endindex = outputlist[targetfieldindex[0] + 1].find('**', beginindex + 2) + 2
                            namemodify = outputlist[targetfieldindex[0] + 1][beginindex:endindex]
                        else:
                            datestr = outputlist[targetfieldindex[0]+1] + ' ' + outputlist[targetfieldindex[0]+2]
                            namestr = (outputlist[targetfieldindex[0] + 1] + ' ' + outputlist[targetfieldindex[0] + 2])
                            beginindex = namestr.find('**')
                            endindex = namestr.find('**', beginindex + 2) + 2
                            namemodify = namestr[beginindex:endindex]
                        dateindex = datestr.find('** on')
                        datestr = datestr[dateindex+6:]
                        period_seconds = timedelta(seconds=90)
                        comment_notification_timestamp = datetime.strptime(datestr, '%d/%b/%y %I:%M %p') + period_seconds
                        comment_buttom_timestamp = datetime.strptime(datestr, '%d/%b/%y %I:%M %p')

                        indices_cmt = [i for i, s in enumerate(outputlist_lowercase) if "%pulse" in s]
                        if len(indices_cmt) != 0:
                            target_field_list.append("comment")

                    # initialize the format of Jira update message

                    if updateflag:
                        Jira_update = Jira_update + "\n" + namemodify + " updated a Jira story" + " (" + Jira_story_key + "):\n"
                    else:
                        Jira_update = Jira_update + "\n" + namemodify + " added comments to a Jira story" + " (" + Jira_story_key + "):\n"

                    # Process field update (field name, message from email header, from & to strings)

                    if updateflag:

                        mylog.info("--> Start Extracting Fields Update: "+ Jira_story_key)

                        url = "https://devtools.cpggpc.ca/jira/rest/api/latest/issue/" + Jira_story_key + "?expand=changelog"

                        headers = {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "Authorization": Jira_basic_auth
                        }
                        response = requests.request(
                            "GET",
                            url,
                            headers=headers
                        )
                        JsonToPython = json.loads(response.text)
                        histories = JsonToPython['changelog']
                        history = histories['histories']

                        # build up updated date and field list 0,2,4,... are date update happened, 1,3,5,... are changed field
                        updatefieldlist = []
                        for i in range(len(history)):
                            updatefieldlist.append(history[i]['created'][:history[i]['created'].find('.')].replace('T', ' '))
                            updatefieldlist.append(str(history[i]['items']))

                        fieldindex = 0
                        while fieldindex<len(target_field_list):
                            target_field = target_field_list[fieldindex]
                            if target_field != "comment":
                                emailitem = [i for i, s in enumerate(target_field_list_notification) if target_field in s]
                                indices = [i for i, s in enumerate(updatefieldlist) if "'field': " + "'" + target_field_list_history[emailitem[0]] in s]
                                itemdictionarylist = []
                                if len(indices) != 0:
                                    target_field_history = target_field_list_history[emailitem[0]]
                                    Jira_update = Jira_update + target_field
                                    i=0
                                    while i <= len(indices)-1:
                                        if (datetime.strptime(updatefieldlist[indices[i]-1],'%Y-%m-%d %H:%M:%S')<= update_notification_timestamp) and (datetime.strptime(updatefieldlist[indices[i]-1],'%Y-%m-%d %H:%M:%S')>= update_buttom_timestamp) :
                                            itemvaluelist = updatefieldlist[indices[i]].replace("'",'"').replace("[","").replace("]","").replace("None",'"None"').replace("},","}&*").split('&*')

                                            # get list of item change values then convert it from string to dictionary type
                                            itemindex = len(itemvaluelist)-1
                                            while itemindex>=0:
                                                pre_checking_str = '"field": "' + target_field_history +'"'
                                                ttt = itemvaluelist[itemindex].find(pre_checking_str)
                                                if itemvaluelist[itemindex].find(pre_checking_str)>=0:
                                                    itemvaluestring = json.loads(itemvaluelist[itemindex])
                                                    if itemvaluestring['field'] in target_field_list_history and itemvaluestring['field']==target_field_list_history[emailitem[0]] :
                                                        Jira_update = Jira_update + "  ->  "
                                                        Jira_update = Jira_update + " from " + str(itemvaluestring['fromString']) + " to " + str(itemvaluestring["toString"])
                                                itemindex=itemindex-1
                                        i=i+1

                            else:
                                # Process comment update (from outputlist - only new comment will be included in field update notification)

                                if commentflag:
                                    # mix comment with fields update
                                    # use regular expression to get the comments from notification
                                    comment_list_indices = [i for i, s in enumerate(outputlist) if namemodify + ' on ' in s]
                                    commentpattern1 = regex.compile(r'\sAM$')
                                    commentpattern2 = regex.compile(r'\sPM$')
                                    commentflag1 = commentpattern1.findall(outputlist[comment_list_indices[0]])
                                    commentflag2 = commentpattern2.findall(outputlist[comment_list_indices[0]])
                                    comment_list_indices.append(len(outputlist)-1)
                                    index1 = 0
                                    while index1 < len(comment_list_indices):
                                        if commentflag1 or commentflag2:
                                            index2 = comment_list_indices[index1] + 1
                                        else:
                                            index2 = comment_list_indices[index1] + 2
                                        temp_comment=''
                                        temp_comment_lower = ''
                                        while index2<len(outputlist) and index2 < comment_list_indices[index1+1]:
                                            temp_comment = temp_comment + outputlist[index2] + " "
                                            temp_comment_lower = temp_comment_lower + outputlist_lowercase[index2] + " "
                                            index2=index2+1
                                        valid_cmt = temp_comment_lower.find('%pulse')
                                        if valid_cmt>=0:
                                            Jira_update = Jira_update + target_field + " -> "
                                            Jira_update = Jira_update + temp_comment + "\n"
                                        index1 = index1 + 1

                            fieldindex = fieldindex + 1
                            Jira_update = Jira_update + "\n"
                    else :

                        # Process comment update only (field name, message from email header, from & to string)

                        mylog.info("--> Start Extracting Comment Update: " + Jira_story_key)

                        url = "https://devtools.cpggpc.ca/jira/rest/api/latest/issue/" + Jira_story_key + "?expand=comments"

                        headers = {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "Authorization": Jira_basic_auth
                        }
                        response = requests.request(
                            "GET",
                            url,
                            headers=headers
                        )
                        JsonToPython = json.loads(response.text)
                        fields = JsonToPython['fields']
                        comment = fields['comment']
                        comments = comment['comments']
                        comments_dict_in_str = json.dumps(comments)
                        comments_lowercase = json.loads(comments_dict_in_str.lower())

                        # build up date,field list 1,3,5 are date update happened, 2,4,6 are field change info
                        updatefieldlist = []
                        indices = []
                        for i in range(len(comments)):
                            itemindex = comments_lowercase[i]['body'].find('%pulse')
                            if itemindex != -1:
                                updatefieldlist.append(comments[i]['updated'][:comments[i]['updated'].find('.')].replace('T', ' '))
                                commentoutput = (h.handle(f'''{str(comments[i]['body'])}''').replace("\\r\\n", ""))
                                updatefieldlist.append(commentoutput)

                        updatefieldlist_lowercase = [item.lower() for item in updatefieldlist]
                        indices = [i for i, s in enumerate(updatefieldlist_lowercase) if "%pulse" in s]
                        target_field = target_field_list[0]
                        Jira_update = Jira_update + target_field + ":\n"
                        if len(indices)!=0:
                            i= len(indices)-1

                            # due to behavior of Jira comments changelog keeps only latest comments (no toString and fromString).only capture latest comments

                            locate_comments = 0
                            while i >= 0 :
                                if (datetime.strptime(updatefieldlist[indices[i] - 1],'%Y-%m-%d %H:%M:%S') <= comment_notification_timestamp) and (datetime.strptime(updatefieldlist[indices[i] - 1],'%Y-%m-%d %H:%M:%S') >= comment_buttom_timestamp):
                                    Jira_update = Jira_update + "(" + updatefieldlist[indices[i]-1] + ") " + updatefieldlist[indices[i]]
                                    locate_comments = 1
                                i = i - 1
                            if locate_comments == 0:
                                Jira_update = ""
                        else:
                            Jira_update = ""

                    mylog.info("--> Body:\t" + str(outputlist))
                    print('Body:\t', outputlist)
                    print('Jira Update:\t',Jira_update)
                    result = mail.uid("STORE", targetemail, '+FLAGS', '\\Seen')
                    #result = mail.copy(targetemail, 'TestJ2P (processed)')
                    mov, data = mail.uid('STORE', targetemail , '+FLAGS', '\\Deleted')
                    PULSE_Jira_Update(Jira_update)
            break

    else:
        print('To:\t\t', email_message['To'])
        print('From:\t', email_message['From'])
        print('Subject:', email_message['Subject'])
        print('Date:\t', email_message['Date'])
        print('Thread-Index:\t', email_message['Thread-Index'])
        text = f"{email_message.get_payload(decode=True)}"
        html = text.replace("b'", "")
        h = html2text.HTML2Text()
        h.ignore_links = True
        output = (h.handle(f'''{html}''').replace("\\r\\n", ""))
        output = output.replace("'", "")
        print(output)

mail.expunge()
mail.close()
mail.logout()