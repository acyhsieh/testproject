#myLog.py
import logging
import getpass
from datetime import datetime

class MyLog(object):
    def __init__(self):
        user=getpass.getuser()
        self.logger=logging.getLogger(user)
        self.logger.setLevel(logging.DEBUG)
        format='%(asctime)s - %(levelname)s -%(name)s : %(message)s'
        formatter=logging.Formatter(format)
        streamhandler=logging.StreamHandler()
        streamhandler.setFormatter(formatter)
        self.logger.addHandler(streamhandler)
        #logfile='../PULSE_JIRA_Integration/' + 'PULSE_Jira_SR_' +datetime.today().strftime('%Y_%m_%d') + '.log'
        logfile= '../PULSE_JIRA_CLOUD_Integration/' + 'PULSE_Jira_CLOUD_' +datetime.today().strftime('%Y_%m_%d') + '.log'
        #logfile = 'PULSE_Jira_' + datetime.today().strftime('%Y_%m_%d') + '.log'
        #logfile = 'Jira_Update_' + datetime.today().strftime('%Y_%m_%d') + '.log'
        #logfile = '../MBR_Health_Check/' + 'MBR_HC_' + datetime.today().strftime('%Y_%m_%d') + '.log'
        #logfile = 'MBR_HC_' + datetime.today().strftime('%Y_%m_%d') + '.log'
        #logfile = '../PULSE_JIRA_Integration/' + 'Jira_Update_' + datetime.today().strftime('%Y_%m_%d') + '.log'
        filehandler=logging.FileHandler(logfile)
        filehandler.setFormatter(formatter)
        self.logger.addHandler(filehandler)
    def debug(self, msg):
        self.logger.debug(msg)
    def info(self, msg):
        self.logger.info(msg)
    def warning(self, msg):
        self.logger.warning(msg)
    def error(self, msg):
        self.logger.error(msg)
    def critical(self, msg):
        self.logger.critical(msg)
    def log(self, level, msg):
        self.logger.log(level, msg)
    def setLevel(self, level):
        self.logger.setLevel(level)
    def disable(self):
        logging.disable(50)